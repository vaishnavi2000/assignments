package com.accenture;
import java.io.*;
import java.util.*;
public class Student {
	private String name;
	private int roll;
public Student (String name,int roll) {
	this.name = name;
	this.roll = roll;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public int getRoll() {
	return roll;
}
public void setRoll(int roll) {
	this.roll = roll;
}
@Override
public String toString() {
	return "Student [name=" + name + ", roll=" + roll + "]";
}


public static void main(String args[]) {
	Student s1 = new Student("vaishnavi",12345);
	Student s2 = new Student("Duddukuri",6789);
	System.out.println(s1);
	System.out.println(s2);
}
}
