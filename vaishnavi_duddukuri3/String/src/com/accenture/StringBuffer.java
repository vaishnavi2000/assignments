package com.accenture;
import java.io.*;

public class StringBuffer {
	public static void main(String args[]) {
		//String Buffer
    String s = new String("Welcome to String Buffer");
	int l = s.length();
	System.out.println(l);
	
	//String Builder
	StringBuilder str = new StringBuilder();
	str.append("Welcome");
	System.out.println("String : " +str.toString());

	StringBuilder str2 = new StringBuilder(10);
	str2.append("ABCDEF");
	System.out.println("String : " +str2.capacity());
	
	
}
}
