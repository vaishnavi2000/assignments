package com.accentureexception;

public class Balance extends RuntimeException {
	public Balance(int n) {
		super(n);
	}
}
