package com.accenture.inheritance;

public class Personal extends EmpName{
	private String dob;
	private int salary;
	public Personal(String empname, String dob, int salary) {
		super(empname);
		this.dob = dob;
		this.salary = salary;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public int getSalary() {
		return salary;
	}
	public void setSalary(int salary) {
		this.salary = salary;
	}
	@Override
	public String toString() {
		return "Personal [dob=" + dob + ", salary=" + salary + ", getDob()=" + getDob() + ", getSalary()=" + getSalary()
				+ ", getEmpname()=" + getEmpname() + ", toString()=" + super.toString() + ", getClass()=" + getClass()
				+ "]";
	}
	
	
	
}

