package com.accenture.inheritance;

public class Professional extends EmpName {
	
	private String Enterpriseid;
	private int empid;
	public Professional(String empname, String enterpriseid, int empid) {
		super(empname);
		Enterpriseid = enterpriseid;
		this.empid = empid;
	}
	public String getEnterpriseid() {
		return Enterpriseid;
	}
	public void setEnterpriseid(String enterpriseid) {
		Enterpriseid = enterpriseid;
	}
	public int getEmpid() {
		return empid;
	}
	public void setEmpid(int empid) {
		this.empid = empid;
	}
	@Override
	public String toString() {
		return "Professional [Enterpriseid=" + Enterpriseid + ", empid=" + empid + ", getEnterpriseid()="
				+ getEnterpriseid() + ", getEmpid()=" + getEmpid() + ", getEmpname()=" + getEmpname() + ", toString()="
				+ super.toString() + ", getClass()=" + getClass() + "]";
	}
	
	
	
	

}
