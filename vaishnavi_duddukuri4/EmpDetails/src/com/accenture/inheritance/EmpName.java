package com.accenture.inheritance;

public class EmpName {
	private String Empname;

	public EmpName(String empname) {
		super();
		Empname = empname;
	}

	public String getEmpname() {
		return Empname;
	}

	public void setEmpname(String empname) {
		Empname = empname;
	}

	@Override
	public String toString() {
		return "EmpName [Empname=" + Empname + ", getEmpname()=" + getEmpname() + ", toString()=" + super.toString()
				+ "]";
	}
	
}
