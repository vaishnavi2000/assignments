package com.accenture.inheritance;

public class Professional extends EmpName {
	
private String enterpriseid;
private int empid;
public Professional(String empname, String enterpriseid, int empid) {
	super(empname);
	this.enterpriseid = enterpriseid;
	this.empid = empid;
}
public String getEnterpriseid() {
	return enterpriseid;
}
public void setEnterpriseid(String enterpriseid) {
	this.enterpriseid = enterpriseid;
}
public int getEmpid() {
	return empid;
}
public void setEmpid(int empid) {
	this.empid = empid;
}
@Override
public String toString() {
	return "Professional [enterpriseid=" + enterpriseid + ", empid=" + empid + ", getEnterpriseid()="
			+ getEnterpriseid() + ", getEmpid()=" + getEmpid() + ", getEmpname()=" + getEmpname() + ", toString()="
			+ super.toString() + ", getClass()=" + getClass() + "]";
}




}
