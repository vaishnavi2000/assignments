package com.accenture;
import java.util.HashSet;
import java.util.Set;
public class Ex1 {
	
	public static void main(String srgs[]) {
		Set<String> s1 = new HashSet<>();
		Set<Integer> s2 = new HashSet<>();
		Set<Integer> s3 = new HashSet<>();
		s1.add("dfs");
		s1.add("dfsf");
		s1.add("kjl");
		s2.add(100);
		s2.add(200);
		s2.add(300);
		s2.add(400);
		s2.add(500);
		s3.add(300);
		s3.add(400);
		s3.add(500);
		
		s2.addAll(s3);
		System.out.println("Union");
		System.out.println(s2);
		
		s2.removeAll(s3);
		System.out.println("Difference");
		System.out.println(s3);
		
		s2.retainAll(s3);
		System.out.println("Intersect");
		System.out.println(s3);
		
		for(String s: s1)
			System.out.print(s + " ");
		System.out.println("");
		System.out.println(".................");
		for(int st: s2)
			System.out.print(st  + " ");
		System.out.println("");
		System.out.println(".................");
		for(int str: s3)
			System.out.print(str  + " ");
		System.out.println("");
		System.out.println(".................");
	}

}
