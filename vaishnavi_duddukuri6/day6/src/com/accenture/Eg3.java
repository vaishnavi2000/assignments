package com.accenture;

import java.lang.Integer;
import java.util.LinkedHashSet;
import java.util.Set;

public class Eg3 {
	public static void main(String srgs[]) {
		Set<String> s1 = new LinkedHashSet<String>();
		Set<String> s4 = new LinkedHashSet<String>();
		Set<Integer> s2 = new LinkedHashSet<Integer>();
		Set<Integer> s3 = new LinkedHashSet<Integer>();
		s1.add("dfs");
		s1.add("abc");
		s1.add("dfsf");
		s1.add("kjl");
		s4.add("acv");
		s4.add("acvfd");
		s4.add("acvew");
		s4.add("abc");
		s2.add(100);
		s2.add(200);
		s2.add(300);
		s2.add(400);
		s2.add(500);
		s3.add(300);
		s3.add(400);
		s3.add(500);
		
		s2.addAll(s3);
		System.out.println("Union");
		System.out.println(s2);
		System.out.println(".................");
		s1.addAll(s4);
		System.out.println("Union");
		System.out.println(s1);
		System.out.println(".................");
		
		s2.removeAll(s3);
		System.out.println("Difference");
		System.out.println(s3);
		System.out.println(".................");
		s1.removeAll(s4);
		System.out.println("Difference");
		System.out.println(s1);
		System.out.println(".................");
		
		s2.retainAll(s3);
		System.out.println("Intersect");
		System.out.println(s3);

		s1.retainAll(s4);
		System.out.println("Intersect");
		System.out.println(s1);
		
		
		for(String s: s1)
			System.out.print(s + " ");
		System.out.println("");
		System.out.println(".................");
		for(int st: s2)
			System.out.print(st  + " ");
		System.out.println("");
		System.out.println(".................");
		for(int str: s3)
			System.out.print(str  + " ");
		System.out.println("");
		System.out.println(".................");
	}

}

